package com.example.sergo.helper;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.content.Intent;

public class Audi_80 extends AppCompatActivity {

    //Создаем массив разделов:
    private String head_array[] = {
            "1. Инструкция по эксплуатации",
            "2. Двигатели, карбюраторы.",
            "3. Системы смазки (4- и 5-цилиндровые модели).",
            "4. Система охлаждения.",
            "5. Топливная система.",
            "6. Выхлопная система.",
            "7. Система зажигания.",
            "8. Трансмиссия.",
            "9. Подвески, колеса.",
            "10. Тормозная система.",
            "11. Рулевое управление.",
            "12. Кузов, салон.",
            "13. Система кондиционирования.",
            "14. Электрооборудование.",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audi_80);

        // Получим идентификатор ListView
        ListView listView = (ListView) findViewById(R.id.listView);
        //устанавливаем массив в ListView
        listView.setAdapter(
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, head_array));
        listView.setTextFilterEnabled(true);

        //Обрабатываем щелчки на элементах ListView:
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(Audi_80.this, MainActivity.class);

                intent.putExtra("head", position);

                //запускаем вторую активность
                startActivity(intent);
            }
        });


    }


}
